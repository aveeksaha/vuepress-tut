module.exports = {
    title: 'New Docs', // Title of the website
    description: "A small documentation site", // appears in the meta tag and as a subtitle
    ga: "Analytics code", // Google Analytics tracking code
    themeConfig: {
        nav: [
            // links that will appear in the top navbar
            { text: 'Guide', link: '/guide/' },
            { text: 'Get Started', link: '/get-started/' },
            { text: 'Github', link: 'https://github.com/Aveek-Saha' }, // external link
        ],
        sidebar: [
            // Create sidebar groups
            {
                title: 'Getting Started',
                collapsable: false,
                children: [
                    '/get-started/installation', // These are pages we'll add later
                    '/get-started/using'
                ]
            },
            {
                title: 'Guide',
                collapsable: false,
                children: [
                    '/guide/api-reference', // These are pages we'll add later
                    '/guide/deploying',
                ]
            }
        ]
    }
}