---
title: Using
---

# Installation

There are two ways to include this in your project.

### NPM
```sh

npm i some-lib

```
### CDN
```sh

https://cdn.jsdelivr.net/npm/something/something.js

```

